﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigHeart : MonoBehaviour
{
	[SerializeField] private SpriteRenderer playerRenderer;
	[SerializeField] private PlayerColorController playerColorController;
	[SerializeField] private float heartGrowthSize;
	[SerializeField] private float heartShrinkSize;
	[SerializeField] private float maxSize;

	private Vector3 initScale;
	private bool grow = false;
	private bool shrink = false;

	// Start is called before the first frame update
	void Start()
	{
		initScale = transform.localScale;
	}

	// Update is called once per frame
	void Update()
	{
		ControlHeartGrowth();
		ControlHeartSize();
	}

	private void ControlHeartGrowth()
	{
		if (playerColorController.RL == RelationshipStatus.Begin) grow = true;
		else if (playerColorController.RL == RelationshipStatus.Idle)
		{
			grow = false;
			// particle effect
		}
		else if (playerColorController.RL == RelationshipStatus.End) shrink = true;
	}

	private void ControlHeartSize()
	{
		Color.RGBToHSV(playerRenderer.color, out float h, out float s, out float v);
		float ratio = s / 1;

		if (grow)
		{
			transform.localScale += new Vector3(heartGrowthSize * ratio,
				heartGrowthSize * ratio, heartGrowthSize * ratio);
			if (transform.localScale.x >= maxSize)
			{
				transform.localScale = new Vector3(maxSize, maxSize, maxSize);
				grow = false;
			}
		}

		if (shrink)
		{
			transform.localScale -= new Vector3(heartShrinkSize * ratio,
				heartShrinkSize * ratio, heartShrinkSize * ratio);
			if (transform.localScale.x <= initScale.x)
			{
				transform.localScale = initScale;
				shrink = false;
			}
		}
	}
}
