﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	[SerializeField] private SpriteRenderer playerRenderer;
	[SerializeField] private ManageEnd ng;
	[SerializeField] private string loseMessage;
	[SerializeField] private string winMessage;

	private void Update()
	{
		LoseGame();
		if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
	}

	private void LoseGame()
	{
		Color.RGBToHSV(playerRenderer.color, out float h, out float s, out float v);
		if (v <= 0) ng.EndAnimation(loseMessage);
	}

	public void WinGame()
	{
		ng.EndAnimation(winMessage);
	}
}
