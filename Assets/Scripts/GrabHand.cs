﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabHand : MonoBehaviour
{
	public bool IsGrabbing { get; private set; }
	public float GrabDistance => grabDistance;

	[SerializeField] private float turnRotation;
	[SerializeField] private float grabFollowSpeed;
	[SerializeField] private float grabDistance;

	[SerializeField] private Transform arms;
	[SerializeField] private Transform legs;
	[SerializeField] private Transform playerHand;
	[SerializeField] private HandHolder handHolder;
	private Animator handAnimator;
	private NPCAI[] npcs;
	private NPCAI npc;
	private PlayerMovement playerMovement;
	private PlayerColorController playerColorController;
	private Coroutine playerCoroutine;
	private Vector3 initHandLocalPos;
	private Quaternion initHandLocalRot;
	private bool grabCoroutineFlag = false;
	private bool letGo = false;
	private bool activateHighlight = true;

	public NPC Npc { get; private set; }

	void Start()
	{
		handAnimator = playerHand.parent.GetComponent<Animator>();
		playerMovement = GetComponent<PlayerMovement>();
		playerColorController = GetComponent<PlayerColorController>();
		npc = null;
		npcs = FindObjectsOfType<NPCAI>();
		initHandLocalPos = playerHand.localPosition;
		initHandLocalRot = playerHand.localRotation;
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetButtonDown("Grab") && FoundNPC()
			&& !IsGrabbing && !grabCoroutineFlag) CallGrabCoroutines();

		if (Input.GetButtonDown("Grab") && IsGrabbing && !playerColorController.CanLetGo) letGo = true;

		if (letGo && npc.name != "Everything") playerColorController.LetGo();

		if (IsGrabbing && playerColorController.CanLetGo)
		{
			ActivateGrab(false);
		}

		if (IsGrabbing && FoundNPC()) FollowHolder();
		else CheckForNPCInRange();
	}

	private bool FoundNPC() => npc != null;

	private void CheckForNPCInRange()
	{

		npc = null;
		Vector3 currentPos = arms.position;
		foreach (NPCAI n in npcs)
		{
			float dist = Vector3.Distance(n.Position, currentPos);
			if (dist < grabDistance)
			{
				if (n != null)
					n.GetComponent<NPC>().ActivateHighlight(true);
				npc = n;
			}
			else if (dist >= grabDistance)
			{
				if (n != null)
					n.GetComponent<NPC>().ActivateHighlight(false);
			}
		}
	}

	private void CallGrabCoroutines()
	{
		grabCoroutineFlag = true;
		playerCoroutine = StartCoroutine(LookAtNpc());
		//npcCoroutine = StartCoroutine(npc.LookAtTarget(arms, turnRotation));
		playerMovement.ActivateMove(false);
	}

	private IEnumerator LookAtNpc()
	{
		while (true)
		{
			Vector3 lookPos = npc.transform.position - arms.position;
			lookPos.y = 0;
			Quaternion rotation = Quaternion.LookRotation(lookPos);
			arms.rotation = Quaternion.Slerp(arms.rotation, rotation, Time.deltaTime * turnRotation);
			legs.rotation = Quaternion.Slerp(legs.rotation, rotation, Time.deltaTime * turnRotation);

			if (Quaternion.Angle(arms.rotation, rotation) < 0.8f && npc.LookingAtPlayer)
			{
				arms.rotation = rotation;
				legs.rotation = rotation;
				if (playerCoroutine != null) StopCoroutine(playerCoroutine);
				//StopCoroutine(npcCoroutine);
				ActivateGrab(true);
			}

			yield return null;
		}
	}

	private void ActivateGrab(bool value)
	{
		bool flag = false;
		if (!value)
		{
			letGo = false;
			playerHand.localPosition = initHandLocalPos;
			playerHand.localRotation = initHandLocalRot;
			SetTag();
			playerColorController.CanShowTag = false;
			handHolder.ActivateHolderFollow(false, npc.Hand.parent);
			npc.ActivateGrab(false);
			handAnimator.enabled = true;
			playerMovement.ActivateMove(true);
			npc = null;
			flag = true;
			grabCoroutineFlag = false;
			IsGrabbing = false;
		}
		if (!flag)
		{
			handAnimator.enabled = false;
			playerColorController.GrabHand(npc.GetComponent<NPC>());
			playerMovement.ActivateMove(true);
			handHolder.ActivateHolderFollow(true, npc.Hand.parent);
			npc.ActivateGrab(true);
			IsGrabbing = true;
		}
	}

	private void SetTag()
	{
		NPC n = npc.GetComponent<NPC>();

		n.ChangeTag(playerColorController.CanShowTag);
		n.ActivateTag(playerColorController.CanShowTag);
	}

	private void FollowHolder()
	{
		playerHand.position = Vector3.MoveTowards(
			playerHand.position,
			handHolder.transform.position,
			grabFollowSpeed * Time.deltaTime);

		npc.Hand.position = Vector3.MoveTowards(
			npc.Hand.position,
			handHolder.transform.position,
			grabFollowSpeed * Time.deltaTime);
	}


}
