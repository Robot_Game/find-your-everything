﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandHolder : MonoBehaviour
{
    [SerializeField] private Transform parent;
    [SerializeField] private Transform playerShoulder;
    [SerializeField] private Transform npcShoulder;
    [SerializeField] private float maxDistance;
    [SerializeField] private float followSpeed;
    private Vector3 initPos;
    //private Quaternion initRot;
    private bool isGrabbing = false;

    // Start is called before the first frame update
    void Start()
    {
        initPos = transform.localPosition;
        //initRot = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (isGrabbing) FollowShoulder();
    }

    public void ActivateHolderFollow(bool value, Transform t)
    {
        SetNpc(t);
        if (value) transform.SetParent(null);
        else
        {
            transform.SetParent(parent);
            transform.localPosition = initPos;
        }
        isGrabbing = value;
    }

    public void SetNpc(Transform t) => npcShoulder = t;

    private void FollowShoulder()
    {
        //Vector3 target = (playerShoulder.position + npcShoulder.position) / 2;
        float yPos = transform.position.y;
        if (Vector3.Distance(transform.position, playerShoulder.position) > maxDistance)
        {
            Vector3 desiredPos = playerShoulder.position;
            Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, followSpeed * Time.deltaTime);
            transform.position = new Vector3(smoothedPos.x, yPos, smoothedPos.z);
            //float angle = Quaternion.Angle(transform.rotation, parent.localRotation);
            //Debug.Log(angle);
            //transform.RotateAround(playerShoulder.position, Vector3.up, angle * Time.deltaTime);
        }


    }
}
