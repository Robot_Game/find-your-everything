﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegController : MonoBehaviour
{

    [SerializeField] private bool isPlayer = false;
    [SerializeField] private float feetMoveSpeed;
    [SerializeField] private float feetLimitLenght = 0.2f;
    [SerializeField] private float feetPlacementOffset = 1;
    [SerializeField] private float stepInterval = 1;

    private PlayerMovement playerController;
    private NPCAI npcController;
    private Transform[] legs;
    private Transform[] feet;
    private Transform currentFoot;
    private Transform currentLeg;
    private Vector3 targetPos;
    private float legsLenght;
    private bool canStep = true;
    private bool canStep2 = true;

    private Coroutine stepMovementCoroutine = null;
    private Coroutine stepIntervalCoroutine = null;

    // Start is called before the first frame update
    void Awake()
    {
        SetInitObjectsAndValues();
    }

    private void SetInitObjectsAndValues()
    {
        if (isPlayer) playerController = FindObjectOfType<PlayerMovement>();
        else npcController = transform.GetComponent<NPCAI>();

        legs = new Transform[2];
        feet = new Transform[2];

        legs[0] = transform.Find("Body").Find("Legs").Find("LLeg");
        legs[1] = transform.Find("Body").Find("Legs").Find("RLeg");
        feet[0] = transform.Find("Feet").Find("LFoot");
        feet[1] = transform.Find("Feet").Find("RFoot");

        currentFoot = feet[0];
        currentLeg = legs[0];
        legsLenght = Vector3.Distance(currentFoot.position, currentLeg.position) + feetLimitLenght;

    }


    private void Update()
    {
        if (FootReachedLimit() && canStep && canStep2)
        {
            CalculateTargetPosition();
            //MoveCurrentFoot();
            stepMovementCoroutine = StartCoroutine(MoveFoot());
            stepIntervalCoroutine = StartCoroutine(StepInterval());
            SwitchCurrentFoot();
        }
    }

    private IEnumerator MoveFoot()
    {
        Transform curFoot = currentFoot;
        Vector3 pos = targetPos;

        while (true)
        {
            canStep = false;
            float step = feetMoveSpeed * Time.deltaTime;
            curFoot.position = Vector3.MoveTowards(curFoot.position, pos, step);
            if (Vector3.Distance(curFoot.position, pos) < 0.1f)
            {
                canStep = true;
                if(stepMovementCoroutine != null) StopCoroutine(stepMovementCoroutine);
            }
            yield return null;
        }
    }

    private IEnumerator StepInterval()
    {
        canStep2 = false;
        yield return new WaitForSeconds(stepInterval);
        canStep2 = true;
        StopCoroutine(stepIntervalCoroutine);
    }

    private bool FootReachedLimit()
        => Vector3.Distance(currentFoot.position, currentLeg.position) > legsLenght;



    private void CalculateTargetPosition()
    {
        Vector3 direction;
        if(!isPlayer) direction = npcController.MoveDirection.normalized;
        else direction = playerController.MoveDirection.normalized;

        targetPos = new Vector3
            (currentLeg.position.x + (direction.x * feetPlacementOffset),
            currentFoot.position.y,
            currentLeg.position.z + (direction.z * feetPlacementOffset));
    }

    private void SwitchCurrentFoot()
    {
        bool flag = false;
        if (currentFoot == feet[0])
        {
            currentFoot = feet[1];
            currentLeg = legs[1];
            flag = true;
        }
        if (currentFoot == feet[1] && flag == false)
        {
            currentFoot = feet[0];
            currentLeg = legs[0];
        }
    }
}
