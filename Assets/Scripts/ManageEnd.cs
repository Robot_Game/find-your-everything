﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ManageEnd : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI endtext;
	private Animator panelAnimator;

	private void Awake()
	{
		panelAnimator = GetComponent<Animator>();
	}

	public void EndAnimation(string endtext)
	{
		this.endtext.text = endtext;
		panelAnimator.SetTrigger("End");
	}

	public void LoadScene() => SceneManager.LoadScene(0);
}
