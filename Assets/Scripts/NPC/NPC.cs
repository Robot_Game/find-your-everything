﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NPC : MonoBehaviour
{
	[SerializeField] private GameObject highlight;
    [SerializeField] private TextMeshPro tagText;
    [SerializeField] private string unknownTagText = "???";
    [SerializeField] private float unknownTagTime = 2f;

    private NPCAI npcAI;

    public Color col;

    public string Name { get; private set; }
    public Color Color { get; private set; }
    public float RelationshipDecreaseSpeed { get; private set; }
    public float HPDecreaseSpeedIncrease { get; private set; }
    public float H { get; private set; }

    private void Awake()
    {
        npcAI = GetComponent<NPCAI>();
    }

    void Start()
    {
        npcAI.Active = true;
    }

	public void ActivateHighlight(bool value) => highlight.SetActive(value);

    public void ActivateTag(bool value) => StartCoroutine(ShowTag(value));

    private IEnumerator ShowTag(bool value)
    {
        bool flag = false;
        if (tagText.enabled) flag = true;

        tagText.enabled = true;
        if (!value && !flag)
        {
            yield return new WaitForSeconds(unknownTagTime);
            tagText.enabled = false;
        }
        StopAllCoroutines();
    }
    public void ChangeTag(bool value)
    {
        if (value)
        {
            tagText.text = name;
        }
        if (!value && !tagText.enabled) tagText.text = unknownTagText;
    }
    public void SetNPCStats(string name, Color color, float relationshipDecreaseSpeed, float hpDecreaseSpeed)
    {
        Name = name;
        Color = color;
        col = Color;
        RelationshipDecreaseSpeed = relationshipDecreaseSpeed;
        HPDecreaseSpeedIncrease = hpDecreaseSpeed;
        tagText.text = name;
        tagText.enabled = false;
    }
}
