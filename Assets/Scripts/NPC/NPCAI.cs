﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAI : MonoBehaviour
{
	public Collider col;
    public enum NPCState { Stopped, Moving, LookingAround }

    [Header("Look Around Stats")]
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float minTimeBeforeLookingAround;
    [SerializeField] private float maxTimeBeforeLookingAround;
    [Header("Move Stats")]
    [SerializeField] private float movementSpeed;
    [SerializeField] private float acceleration = 0.15f;
    [SerializeField] private float minWalkTime;
    [SerializeField] private float maxWalkTime;
    [SerializeField, Range(0, 1)] private float walkProbability;
	[SerializeField] private float viewDistance;
    [Header("PlayerDetection")]
    [SerializeField] private float turnToPlayerSpeed;
    [SerializeField] private float detectionRadius;
    [SerializeField] private float followDistance;

    private Transform player;
    private Animator[] armAnimators;
    private NPCState npcState = NPCState.Stopped;
    private Rigidbody rb;
    private Quaternion finalRot;
    private Quaternion initHandLocalRot;
    private Vector3 initHandLocalPos;
    private Vector3 currentPos;
    private Vector3 lastPos;
    private Transform arms;
    private Transform legs;
    private Transform body;
    private float currentMovementSpeed;
    private float finalWalkTime = 0;
    private float finalTimeBeforeLookAround = 0;
    private float walkTimer = 0;
    private float lookAroundTimer = 0;
    private bool randomWalkTime = true;
    private bool randomTimeBeforeLookAround = true;
    private bool isMoving = false;

    public Vector3 MoveDirection { get; private set; } = Vector3.zero;
    public Vector3 Position { get; private set; }
    public Transform Hand { get; private set; }
    public bool Active { get; set; }
    public bool LookingAtPlayer { get; set; } = false;
    public bool IsGrabbing { get; set; } = false;

    void Awake()
    {

        player = FindObjectOfType<PlayerMovement>().transform.Find("Body").Find("Arms").transform;
        body = transform.Find("Body");
        arms = body.Find("Arms");
        legs = body.Find("Legs");
        Hand = arms.Find("RArm").Find("RHand");
        rb = body.GetComponent<Rigidbody>();
        armAnimators = new Animator[2];
        armAnimators[0] = arms.GetChild(0).GetComponent<Animator>();
        armAnimators[1] = arms.GetChild(1).GetComponent<Animator>();
        initHandLocalPos = Hand.localPosition;
        initHandLocalRot = Hand.localRotation;
    }

    private void Start()
    {
        currentMovementSpeed = 0;
        GetPosition();
    }

    void Update()
    {
        if (Active)
        {
			if (IsInRadius() || IsGrabbing)
            {
                if (!IsGrabbing) isMoving = false;
                LookAtObj();
            }
            else LookingAtPlayer = false;

            if (!LookingAtPlayer)
            {
                if (npcState == NPCState.Stopped) GetRandomRotation();
                if (npcState == NPCState.LookingAround) LookAround();
            }

            if (!IsGrabbing) SetAnimationStatus();
        }

    }

    private void FixedUpdate()
    {
        if (Active)
            if (npcState == NPCState.Moving && !IsGrabbing && !IsInRadius()) RandomMove();
        if (IsGrabbing) FollowPlayer();
    }

    private Vector3 GetPosition() => Position = body.position;
    private void LookAtObj()
    {
        Vector3 lookPos = player.position - arms.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        arms.rotation = Quaternion.Slerp(arms.rotation, rotation, Time.deltaTime * turnToPlayerSpeed);
        legs.rotation = Quaternion.Slerp(legs.rotation, rotation, Time.deltaTime * turnToPlayerSpeed);
        LookingAtPlayer = true;

    }

    private bool IsInRadius() => Vector3.Distance(player.position, Position) < detectionRadius;
    public IEnumerator LookAtTarget(Transform target, float turnRotation)
    {
        while (true)
        {
            Vector3 lookPos = target.transform.position - arms.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            arms.rotation = Quaternion.Slerp(arms.rotation, rotation, Time.deltaTime * turnRotation);
            legs.rotation = Quaternion.Slerp(legs.rotation, rotation, Time.deltaTime * turnRotation);

            if (Quaternion.Angle(arms.rotation, rotation) < 0.8f)
            {
                arms.rotation = rotation;
                legs.rotation = rotation;
                LookingAtPlayer = true;
            }

            yield return null;
        }
    }

    public void ActivateGrab(bool value)
    {
        if (!value)
        {
            Hand.localPosition = initHandLocalPos;
            Hand.localRotation = initHandLocalRot;
        }
        armAnimators[1].enabled = !value;
        IsGrabbing = value;
    }

    private void GetRandomRotation()
    {
        finalRot = Random.rotation;
        finalRot.eulerAngles = new Vector3(0, finalRot.eulerAngles.y, 0);
        npcState = NPCState.LookingAround;
    }

    private void GetRandomWalkTime()
    {
        if (randomWalkTime)
        {
            finalWalkTime = Random.Range(minWalkTime, maxWalkTime);
            randomWalkTime = false;
        }
    }

    private void RandomMove()
    {
        isMoving = true;
		CheckForWall();
		GetRandomWalkTime();
        ControlNPCSpeed();
        currentPos = GetPosition();
        rb.MovePosition(body.position + arms.forward * currentMovementSpeed * Time.fixedDeltaTime);
        MoveDirection = currentPos - lastPos;
        lastPos = GetPosition();
	}

    private void FollowPlayer()
    {
        isMoving = true;

        currentPos = GetPosition();

        rb.MovePosition(body.position + arms.forward * currentMovementSpeed * Time.fixedDeltaTime);

        if (Vector3.Distance(arms.position, player.position) < followDistance) StopMoving();
        else Accelerate();

        MoveDirection = currentPos - lastPos;

        lastPos = GetPosition();
    }

    private void SetAnimationStatus()
    {
        foreach (Animator a in armAnimators)
            a.SetBool("IsMoving", isMoving);
    }

    private void ControlNPCSpeed()
    {
        walkTimer += Time.fixedDeltaTime;
        if (walkTimer >= finalWalkTime) StopMoving();
        else Accelerate();
    }

    private void Accelerate()
    {
        currentMovementSpeed += acceleration;
        if (currentMovementSpeed >= movementSpeed) currentMovementSpeed = movementSpeed;
    }

    private void StopMoving()
    {
        currentMovementSpeed -= acceleration;
        if (currentMovementSpeed <= 0)
        {
            currentMovementSpeed = 0;
            walkTimer = 0;
            randomWalkTime = true;
            npcState = NPCState.Stopped;
            MoveDirection = Vector3.zero;
            isMoving = false;
        }
    }

    private void LookAround()
    {
        GetRandomLookAroundTime();
        lookAroundTimer += Time.deltaTime;
        if (lookAroundTimer >= finalTimeBeforeLookAround)
        {
            arms.rotation = Quaternion.Slerp(arms.rotation, finalRot, rotationSpeed);
            legs.rotation = Quaternion.Slerp(legs.rotation, finalRot, rotationSpeed);
            if (Quaternion.Angle(arms.rotation, finalRot) < 0.2f)
            {
                lookAroundTimer = 0;
                arms.rotation = finalRot;
                legs.rotation = finalRot;
                randomTimeBeforeLookAround = true;
                if (Random.Range(0f, 1f) < walkProbability) npcState = NPCState.Moving;
                else
                {
                    npcState = NPCState.Stopped;
                    MoveDirection = Vector3.zero;
                }
            }
        }
    }

    private void GetRandomLookAroundTime()
    {
        if (randomTimeBeforeLookAround)
        {
            finalTimeBeforeLookAround = Random.Range(minTimeBeforeLookingAround, maxTimeBeforeLookingAround);
            randomTimeBeforeLookAround = false;
        }
    }

	private void CheckForWall()
	{
		Ray ray = new Ray(body.position, MoveDirection);
		Debug.DrawRay(body.position, MoveDirection, Color.green);
		if(Physics.Raycast(ray, out RaycastHit hit, viewDistance))
		{
			if (hit.collider.gameObject.tag == "Collider")
			{
				col = hit.collider;
				isMoving = false;
				npcState = NPCState.Stopped;
				currentMovementSpeed = 0;
				MoveDirection = Vector3.zero;
				randomWalkTime = true;
				walkTimer = 0;
			}
		}
	}
}
