﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCManager : MonoBehaviour
{
	[Header("Spawning")]
	[SerializeField] private Vector2 spawnArea;
	[SerializeField] private int minNumberOfNpcs;
	[SerializeField] private int maxNumberOfNpcs;
	[SerializeField] private GameObject NPCPrefab;
	[SerializeField, Range(0, 1)] private float[] colorSpawnProbabilities;
	[SerializeField, Range(0, 0.5f)] private float[] colorTierValues;
	[SerializeField] private string[] tierNames;
	[SerializeField] private float[] tierRelationshipDecreaseSpeeds;
	[SerializeField] private float[] tierHpDecreaseSpeeds;

	public Color winningColor;
	private Color finalColor;
	private string tierName;
	private float colorToSpawnHue;
	private float relationshipDecreaseSpeed;
	private float HpDecreaseSpeed;
	private int numberOfNpcs;
	private bool spawnedWinner;
	public float[] winningColorHSV { get; } = new float[3];

	private void Awake()
	{
		SetWinningColor();
		StartNPCSpawner();
	}

	private void StartNPCSpawner()
	{
		numberOfNpcs = Random.Range(minNumberOfNpcs, maxNumberOfNpcs);
		for (int i = 0; i < numberOfNpcs; i++)
			SpawnNPC();
	}

	private void SpawnNPC()
	{
		SpawnWinnerNPC();

		float prob = Random.Range(0f, 1f);
		if (prob < colorSpawnProbabilities[0])
			SetNPCStats(0);
		else if (prob > colorSpawnProbabilities[0] && prob < colorSpawnProbabilities[1])
			SetNPCStats(1);
		else if (prob > colorSpawnProbabilities[1])
			SetNPCStats(2);

		GameObject go = Instantiate(NPCPrefab, GetRandomPosition(), Quaternion.identity);
		Quaternion rot = GetRandomRotation();
		go.transform.Find("Body").Find("Legs").rotation = rot;
		go.transform.Find("Body").Find("Arms").rotation = rot;
		go.GetComponent<NPC>().SetNPCStats(tierName, finalColor, relationshipDecreaseSpeed, HpDecreaseSpeed);
		if (go.GetComponent<NPC>().Color == Color.black) go.transform.position = new Vector3(500, 500, 500);
		go.name = tierName;
	}

	private Vector3 GetRandomPosition()
	{
		float randomX = Random.Range(-spawnArea.x, spawnArea.x);
		float randomZ = Random.Range(-spawnArea.y, spawnArea.y);

		Vector3 randomPos = new Vector3(randomX, 0.152f, randomZ);

		return randomPos;
	}

	private Quaternion GetRandomRotation()
	{
		Quaternion randomRot = Random.rotation;
		randomRot.eulerAngles = new Vector3(0, randomRot.eulerAngles.y, 0);
		return randomRot;
	}

	private void SetNPCStats(int tierValue)
	{
		float sideProb = Random.Range(0f, 1f);
		if (sideProb > 0.5f) colorToSpawnHue = winningColorHSV[0] + colorTierValues[tierValue];
		else colorToSpawnHue = winningColorHSV[0] - colorTierValues[tierValue];

		if (colorToSpawnHue < 0) colorToSpawnHue = Mathf.Abs(1 - colorToSpawnHue);
		if (colorToSpawnHue > 1) colorToSpawnHue = Mathf.Abs(1 + colorToSpawnHue);

		finalColor = Color.HSVToRGB(colorToSpawnHue, 1, 1);

		tierName = tierNames[tierValue];
		relationshipDecreaseSpeed = tierRelationshipDecreaseSpeeds[tierValue];
		HpDecreaseSpeed = tierHpDecreaseSpeeds[tierValue];
	}

	private void SpawnWinnerNPC()
	{
		if (!spawnedWinner)
		{
			GameObject go = Instantiate(NPCPrefab, GetRandomPosition(), Quaternion.identity);
			Quaternion rot = GetRandomRotation();
			go.transform.Find("Body").Find("Legs").rotation = rot;
			go.transform.Find("Body").Find("Arms").rotation = rot;
			go.name = "Everything";
			go.GetComponent<NPC>().SetNPCStats(tierNames[tierNames.Length - 1], winningColor, 0f, -0.02f);
			spawnedWinner = true;
		}
	}

	private void SetWinningColor()
	{
		winningColor = Random.ColorHSV();
		Color.RGBToHSV(winningColor, out winningColorHSV[0], out winningColorHSV[1], out winningColorHSV[2]);
		winningColor[1] = 1;
		winningColor[2] = 1;
		winningColor = Color.HSVToRGB(winningColorHSV[0], winningColor[1], winningColor[2]);
	}

	void OnDrawGizmosSelected()
	{
		// Draw a semitransparent red cube at the transforms position
		Gizmos.color = new Color(1, 0, 0, 1);
		Gizmos.DrawCube(transform.position, new Vector3(spawnArea.x * 2, 0, spawnArea.y * 2));
	}

}
