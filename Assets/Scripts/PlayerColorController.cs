﻿using UnityEngine;
using UnityEngine.UI;

public enum RelationshipStatus
{
	None, Begin, Idle, End
}

public class PlayerColorController : MonoBehaviour
{
	[SerializeField] private SpriteRenderer playerRenderer;
	[SerializeField] private Image HPBar;
	[SerializeField] private float relationshipIncreaseRate;
	[SerializeField] private float brightnessDecreaseRate;
	[SerializeField] private float minSaturationToShowTag = 0.5f;

	private GameManager gm;
	private NPC grabbedNPC;
	private Color desiredColor;
	private bool grabbed;
	private bool getColor;
	private RelationshipStatus relationshipStatus;
	private float hue;
	private float saturation;
	private float value;
	private float finalBrightnessDecreaseRate;

    public bool CanLetGo => !grabbed;
    public bool CanShowTag { get; set; }

	public RelationshipStatus RL => relationshipStatus;

	private void Start()
	{
		gm = FindObjectOfType<GameManager>();
		finalBrightnessDecreaseRate = brightnessDecreaseRate;
		relationshipStatus = RelationshipStatus.None;
	}

	void Update()
	{
		ChangePlayerHP();
		if (grabbed) ChangePlayerColor();
	}

	public void GrabHand(NPC n)
	{
        grabbedNPC = n;
		grabbed = true;
		getColor = true;
        relationshipStatus = RelationshipStatus.Begin;
	}

	public void LetGo()
	{
		if (relationshipStatus == RelationshipStatus.Begin ||
			relationshipStatus == RelationshipStatus.Idle ||
            relationshipStatus == RelationshipStatus.End)
		{
            relationshipStatus = RelationshipStatus.End;
            if (saturation <= 0)
			{
				grabbed = false;
				getColor = false;
                relationshipStatus = RelationshipStatus.None;
				finalBrightnessDecreaseRate = brightnessDecreaseRate;
			}
		}
	}

	private void ChangePlayerColor()
	{
		if (getColor)
		{
			desiredColor = grabbedNPC.Color;
			getColor = false;
		}
		Color.RGBToHSV(desiredColor, out float h, out float s, out float v);
		ChangePlayerHue(h);
		ChangePlayerSaturation(s);
		Color newColor = Color.HSVToRGB(hue, saturation, value);
		playerRenderer.color = newColor;
	}

	private void ChangePlayerHue(float h) => hue = h;

	private void ChangePlayerSaturation(float s)
	{
        if (saturation <= s && relationshipStatus == RelationshipStatus.Begin)
		{
            saturation += relationshipIncreaseRate * Time.deltaTime;
            if (saturation > minSaturationToShowTag) CanShowTag = true;
        }
		if (saturation >= s && relationshipStatus == RelationshipStatus.Begin)
		{
			if (grabbedNPC.Name == "Everything") gm.WinGame();
            finalBrightnessDecreaseRate += grabbedNPC.HPDecreaseSpeedIncrease;
			relationshipStatus = RelationshipStatus.Idle;
		}

		if (relationshipStatus == RelationshipStatus.End)
            saturation -= grabbedNPC.RelationshipDecreaseSpeed * Time.deltaTime;

        if (saturation < 0) saturation = 0;
        if (saturation > 1) saturation = 1;
    }

	private void ChangePlayerHP()
	{
        Color.RGBToHSV(playerRenderer.color, out float h, out float s, out value);
        value -= finalBrightnessDecreaseRate * Time.deltaTime;
        Color newColor = Color.HSVToRGB(hue, saturation, value);
        playerRenderer.color = newColor;
	}
}
