﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Vector3 MoveDirection { get; private set; } = Vector3.zero;
    [SerializeField] private float movementSpeed = 2f;
    [SerializeField] private float rotationSpeed = 0.15f;
    [SerializeField] private float acceleration = 0.15f;

    private GrabHand grabHand;
    private Animator[] armAnimators;
    private Transform arms;
    private Transform legs;
    private Transform body;
    private Rigidbody rb;
    private Vector3 currentVelocity;
    private bool isMoving = false;
    private bool canMove = true;

    // Start is called before the first frame update
    void Start()
    {
        SetInitObjectsAndValues();
    }

    private void SetInitObjectsAndValues()
    {

        grabHand = GetComponent<GrabHand>();
        body = transform.GetChild(0);
        arms = transform.Find("Body").Find("Arms");
        legs = transform.Find("Body").Find("Legs");
        armAnimators = new Animator[2];
        armAnimators[0] = arms.GetChild(0).GetComponent<Animator>();
        armAnimators[1] = arms.GetChild(1).GetComponent<Animator>();
        rb = body.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        isMoving = ReadMovementInput();
        if(!grabHand.IsGrabbing) SetAnimationStatus();
    }

    private void FixedUpdate()
    {
        if (isMoving && canMove)
        {
            Move();
            Rotate();
        }
        //else StopMoving();
    }

    private bool ReadMovementInput()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        MoveDirection = new Vector3(moveHorizontal, 0.0f, moveVertical).normalized;

        return MoveDirection != Vector3.zero ? true : false;
    }

    public void ActivateMove(bool value)
    {
        canMove = value;
    }

    private void Move()
    {
        rb.MovePosition(body.position + MoveDirection * movementSpeed * Time.fixedDeltaTime);
        //transform.Translate(moveDirection * movementSpeed * Time.fixedDeltaTime);
        //rb.AddForce(moveDirection * movementSpeed, ForceMode.Acceleration);
    }

    private void Rotate()
    {
        arms.rotation = Quaternion.Slerp(
            arms.rotation, Quaternion.LookRotation(MoveDirection), rotationSpeed);
        legs.rotation = Quaternion.Slerp(
            legs.rotation, Quaternion.LookRotation(MoveDirection), rotationSpeed);
        //transform.rotation = Quaternion.LookRotation(moveDirection);
    }

    private void StopMoving()
    {
        currentVelocity = rb.velocity;

        float xSpeed = currentVelocity.x - acceleration;
        float zSpeed = currentVelocity.z - acceleration;

        if (xSpeed < 0) xSpeed = 0;
        if (zSpeed < 0) zSpeed = 0;

        rb.velocity = new Vector3(xSpeed, 0, zSpeed);
    }

    private void SetAnimationStatus()
    {
        foreach (Animator a in armAnimators)
            a.SetBool("IsMoving", isMoving && canMove);
    }
}
