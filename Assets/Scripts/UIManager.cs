﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
	[Header("HP Slider")]
	[SerializeField] private SpriteRenderer playerRenderer;
	[SerializeField] private Slider slider;

	private void Update()
	{
		SlideSlider();
	}

	public void SlideSlider()
	{
		Color.RGBToHSV(playerRenderer.color, out float h, out float s, out float v);
		slider.value = v;
	}
}
